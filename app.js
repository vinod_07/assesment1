const express = require('express');
const app = express();
const port = 4002;

// Set up middleware to parse form data
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public')); // Serve static files from the 'public' directory

// Homepage route
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Form submission route
app.post('/submit', (req, res) => {
  const formData = req.body;
  // Handle the form data as needed (e.g., store in a database, log, etc.)
  console.log('Form Data:', formData);
  res.send('Thank You..! Your Response has Been Submitted Successfully');
});

// Start the server
app.listen(port, () => {
  console.log(`Your Application is Running on http://localhost:${port}`);
});
